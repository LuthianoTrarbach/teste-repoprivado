package ${package}.adapter;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.cloud.stream.test.matcher.MessageQueueMatcher.receivesPayloadThat;

import java.util.concurrent.BlockingQueue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.test.context.junit4.SpringRunner;

import ${package}.App;
import ${package}.config.broker.BrokerOutput;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = WebEnvironment.MOCK)
public class AdapterSaidaBrokerTest {

	@Autowired
	BrokerOutput output;
	
	@Autowired
	private MessageCollector collector;

	@Test
	public void testSendMessage() {	
	 
		//cria a mensagem a ser testada
		Message<?> message = MessageBuilder.withPayload("teste").build();

		//envia a mensagem
		this.output.outPutBroker().send(message);
		
		//lê a mensagem postada
		BlockingQueue<Message<?>> messages = collector.forChannel(output.outPutBroker());
		
		//verifica se são iguais
		assertThat(messages, receivesPayloadThat(is("teste")));
	}
	
}
