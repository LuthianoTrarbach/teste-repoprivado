#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ${package}.domain.Sample;

@Repository
public interface SampleRepository extends MongoRepository<Sample, String> {

}
