package ${package}.config.broker;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({BrokerInput.class, BrokerOutput.class})
public class BrokerConfig {

}
