# Credenciamento Digital Microservice Archetype

## Tecnologias
- Spring-boot
- MongoDB

## Pré-requisitos
- Java (versão 8 ou mais atual)
- Maven (versão 3 ou mais atual)

## Instalação no repositório local
```
mvn clean install
```

## Gerar projeto de micro serviço
```
mvn archetype:generate -B \
    -DarchetypeGroupId=br.com.getnet.credenciamentodigital.archetype \
    -DarchetypeArtifactId=microservice-archetype \
    -Dversion=1.0.0 \
    -DgroupId=br.com.getnet.credenciamentodigital.sample \
    -DartifactId=sample-microservice
```

### Parâmetros
- groupId: é o nome do groupId do projeto maven.
- artifactId: é o nome do artifactId do projeto maven.
